// An example configuration file.
exports.config = {
  directConnect: true,

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome'
  },

  // Framework to use. Jasmine is recommended.
  framework: 'jasmine',

  // Spec patterns are relative to the current working directory when
  // protractor is called.
  specs: ['../tests/calculator.js'],

  // Options to be passed to Jasmine.
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }
};
//https://devhints.io/jasmine
//https://www.protractortest.org/#/api?view=ProtractorBrowser.prototype.get
//https://www.youtube.com/watch?v=u_awobQ1fws&list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&index=7